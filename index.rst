.. LambdaFAQ documentation master file, created by
   sphinx-quickstart on Sun Jul 28 11:28:31 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=======================================
Maurice Naftalin's Lambda FAQ - ja
=======================================

OpenJDKの `Project Lambda <http://openjdk.java.net/projects/lambda/>`_ のページにもリンクされている、Maurice Naftalin氏が作成したプロジェクトラムダに関するFAQ集です。

原文は `こちら <http://www.lambdafaq.org/lambda-resources/>`_ にあります。

About the Lambda FAQ
-----------------------

..  The long debate about how to introduce lambda expressions (aka closures) into Java is approaching an important moment: 
    the implementation of lambdas and virtual extension methods is now feature-complete, 
    and is officially due to be shipped in JDK 8 in March 2014. 
    The biggest changes in the language since Java 5—at least—are not far away now.
    
    I’ve started thinking about the new features, partly so that Phil Wadler and I could consider
    a new edition of our book Java Generics and Collections. 
    The material there, especially the material about collections,
    will need a lot of changes to keep it current when lambda expressions become widely used.
    
    The new features are not all easy to understand at first, so this FAQ is intended to give you the benefit of my labour in learning about them. 
    I hope you find it useful, whether you are already familiar with
    lambda expressions or encountering them for the first time. 
    All comments and contributions are welcome. I’m already pleased to acknowledge:

Javaに対してどのようにラムダ式 (クロージャ) を導入するかについて、長い間議論が交わされてきましたが、
ついにラムダ実装および仮想拡張メソッドが feature-complete - 仕様確定 され、2014年3月にJDK8として公式にリリースされます。
言語仕様の大きな変更は、少なくとも Java 5 以来の出来事です。

この新しい機能について検討を始めたとき、フィリップ・ワルダーと私は、以前共著した書籍 Java Generics and Collections を
改版しようと考えました。
"ネタ"について、特にコレクションに関しては、ラムダ式が広く使われるにあたって多くの内容を変更する必要があると思いました。

新しい機能を理解するのは最初は簡単ではありません。このFAQは私自身がラムダを苦労して学んだ経験を伝えたいと思っています。
既にラムダ式に精通している方も、偶然この記事をご覧になった方にも役立つことを願っています。
多くのコメントや記事の投稿を歓迎しています。コメントや記事投稿に関する貢献は、既に以下のようなものがあります:


.. * the continuing input from the Oracle Java Language and Tools team, especially Stuart Marks;
     their collaboration is helping to greatly improve the scope and accuracy of this document;
   
   * Raoul-Gabriel Urma, with whom I’ve had many interesting discussions and who is making a very helpful contribution to the answers.
     
* | OracleのJava言語チームおよび、ツールチーム、特にスチュアート・マークスから多くの情報を頂いています。
  | 各チームとのコラボレーションによって、このドキュメントのスコープや正確さの向上に繋がっています。

* ラウル・ガブリエル ウルマには、多くの有意義な議論によって様々な有効な答えに導いてくれました。


.. (Of course, all errors and omissions are my responsibility.) I will be happy to include and acknowledge your contribution too.
   But please be aware that anything on this website may end up as part of the material of a new edition of Java Generics and Collections.

| (もちろん、全ての誤りや漏れは私の責任です。) みなさんの反響や協力を楽しみにしています。
| しかし、このWebサイトの内容が書籍 Java Generics and Collections. の改版時の"ネタ"となることをご了承ください。



What's the Purpose of this FAQ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. The question-and-answer format is intended to serve two purposes;
   
   * It’s a tutorial: if you’re starting from scratch in learning about the upcoming changes to Java,
     you should be able to start with the first question, “What is a lambda expression?”
     and get a tutorial introduction by following the “next” link at the top right of each page;
    
   * It’s a reference: if you want an answer to a particular technical question, jump straight to it. 
     Since the Oracle team are also reading these answers,
     it’s reasonable to expect that this will become an authoritative popular reference to what’s going to happen.
    
     * As part of the reference material, there’s a resources page,
       where I will maintain up-to-date links with the various rapidly-changing useful resources: documentation, 
       presentations, build and download resources, tool support and mailing lists.


この文書をQ&A形式にしたことには、2つの目的があります;

* | チュートリアルとして :
  | Javaの新機能について手探りで学び始めようとした場合、まず最初に一つの疑問にぶつかるでしょう。
  | "ラムダ式って何？" 以下のリンクから、この疑問に解決するチュートリアルをはじめましょう。


* | リファレンスとして :
  | 特定の技術的な疑問に対する答えを探している場合、直接該当のページを確認することもできます。
  | Oracleのチームでさえも、各ページを参照して疑問を解決しています。
  | 何か困ったことが見つかったとき、このドキュメントは信頼できるリファレンスとなるでしょう。                       


.. The tutorial sequence is divided into topics, to be read in the sequence listed below and in the left-hand sidebar on every page,
   which also corresponds to the “next” and “previous” links at the top of each post. An exception is the topic of “Design Rationale”;
   some questions here can also be found in the tutorial sequence, others are free-standing.

チュートリアルはトピックごとに分けられており、全てのページの左サイドバーに順番に並べられています。
また、各ページには"次へ"と"戻る"リンクがページトップと下部にあります。"設計根拠"に関するトピックのみは例外でリンクはありません。
多くの疑問はチュートリアルを順番に呼んでいくか、また特定のページを直接参照することで解決するでしょう。



Whats' Your Question?
^^^^^^^^^^^^^^^^^^^^^^^

.. Let me know what you think of this resource, how it could be improved, 
   and what questions you would like to see answered here. Ask the FAQ!
   
   1: Fundamentals of Lambda Expressions

このドキュメントについて思ったことや、もっとこうしたらいいのでは？といったご意見、および掲載してほしい
質問とその回答について募集しています。FAQに聞いてみよう!


1. ラムダ式の基礎
"""""""""""""""""""

.. * What is a lambda expression?
   * Why are lambda expressions being added to Java?
   * What is functional interface?
   * What is the type of a lambda expression?
   * Are lambda expressions objects?
   * Where can lambda expressions be used?
   * What are the scoping rules for lambda expression?
   * Can lambda expressions be used to define recursive functions?
   * Can lambda expressions use variables from their environment?
   * What are method references?
   * What is the syntax for instance method references?
   * What are constructor references?


.. toctree::
   :maxdepth: 1 

    ラムダ式とは何ですか                                   <01_LambdaExpression/WhatIsALambdaExpression>
    何故Javaにラムダ式を追加するのですか                   <01_LambdaExpression/WhyAreLambdaExpressionBeingAddedToJava>
    functional interface とは何ですか                      <01_LambdaExpression/WhatIsAFunctionalInterface>
    ラムダ式の型は何ですか                                 <01_LambdaExpression/WhatIsTheTypeOfALambdaExpression>
    ラムダ式はオブジェクトですか                           <01_LambdaExpression/AreLambdaExpressionsObjects>
    ラムダ式はどこで使うことができますか                   <01_LambdaExpression/WhereCanLambdaExpressionsBeUsed>
    ラムダ式のスコープのルールはどうなっていますか         <01_LambdaExpression/WhatAreTheScopingRulesForLambdaExpressions>
    ラムダ式で再帰的な関数を定義することはできますか       <01_LambdaExpression/CanLambdaExpressionsBeUsedToDefineRecursiveFunctions>
    ラムダ式では環境に応じた変数を扱うことは可能ですか     <01_LambdaExpression/CanLambdaExpressionsUseVariablesFromTheirEnvironment>
    メソッド参照とは何ですか                               <01_LambdaExpression/WhatAreMethodReferences>
    インスタンス参照メソッドはどのような文法で表現しますか <01_LambdaExpression/WhatIsTheSyntaxForInstanceMethodReference>
    コンストラクタ参照とは何ですか                         <01_LambdaExpression/WhatAreConstructorReferences>


.. 2: Default Methods
   
   * What are default methods?
   * Do default methods introduce multiple inheritance to Java?
   * How are conflicting method declarations resolved?
   * What about the diamond problem? 


2. デフォルトメソッド
"""""""""""""""""""""""""

.. toctree::
   :maxdepth: 1

   デフォルトメソッドとは何ですか                             <02_DefaultMethods/WhatAreDefaultMethods> 
   デフォルトメソッドによりJavaにも多重継承が導入されますか   <02_DefaultMethods/DoDefaultMethodIntroduceMultipleInheritanceToJava>
   どのように競合するメソッドを解決しますか                   <02_DefaultMethods/HowAreConflictingMethodDeclarationsResolved>
   ダイヤモンド問題とは何ですか                               <02_DefaultMethods/WhatAboutTheDiamondProblem>
  


.. 3: Collections
  
  * Where is the Java Collections Framework going?
  * What is a stream?


3. コレクション
""""""""""""""""""""""

.. toctree::
   :maxdepth: 1

   Javaコレクションフレームワークはどこに向かっていますか    <03_Collections/WhereIsTheJavaCollectionFrameworkGoing>
   ストリームとは何ですか                                    <03_Collections/WhatIsStream>


.. Design Rationale
  
  * Why are lambda expressions being added to Java?
  * Are lambda expressions objects?
  * Why the restriction on local variable capture?
  * Where is the Java Collections Framework going?
  * Why are Stream operations not defined directly on Collection?
  * Why are lambda expressions so-called?



設計原則に関する質問
""""""""""""""""""""""

.. toctree::
   :maxdepth: 1

   なぜJavaにラムダ式を追加するのですか?                       <01_LambdaExpression/WhyAreLambdaExpressionBeingAddedToJava>
   ラムダ式はオブジェクトですか？                              <01_LambdaExpression/AreLambdaExpressionsObjects>
   なぜローカル変数へのアクセスには制約があるのですか?         <DesignRationale/WhyTheRestrictionOnLocalVariableCapture>
   Javaコレクションフレームワークはどこに向かっていますか      <03_Collections/WhereIsTheJavaCollectionFrameworkGoing>
   なぜStreamに関する操作はCollectionに直接定義しないのですか  <DesignRationale/WhyAreStreamOperationsNotDefinedDirectlyOnCollection>
   なぜラムダ式と呼ぶのですか?                                 <DesignRationale/WhyAreLambdaExpressionsSoCalled>
   なぜデフォルトメソッドには予約語が必要ですか?               <DesignRationale/WhyDoDefaultMethodNeedAKeyword>


..  Advanced Questions
  
  * Lambdas and closures — what’s the difference?


応用的な質問
"""""""""""""""""""

.. toctree::
   :maxdepth: 1

   ラムダとクロージャ - 違いはなんですか?    <AdvancedQuestions/LambdaAndClosures>

