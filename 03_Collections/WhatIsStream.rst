.. What is a stream?

=====================================
ストリームとはなんですか
=====================================


..  
  A stream is a sequence of values. 
  The package java.util.stream defines types for streams of reference values (Stream)
  and some primitives (IntStream, LongStream, and DoubleStream).
  Streams are like iterators in that they yield their elements as required for processing,
  but unlike them in that they are not associated with any particular storage mechanism.
  A stream is either partially evaluated—some of its elements remain to be generated—or exhausted, 
  when its elements are all used up.
  A stream can have as its source an array, a collection, a generator function, or an IO channel;
  alternatively, it may be the result of an operation on another stream (see below). 
  A partially evaluated stream may have infinitely many elements still to be generated,
  for example by a generator function.

ストリームとは、一続きの連続した値のことです。
パッケージ `java.util.stream <http://lambdadoc.net/api/java/util/stream/package-summary.html>`_ には、
参照型値に対応したストリーム ( `Stream <http://lambdadoc.net/api/java/util/stream/Stream.html>`_ )と
プリミティブ型に対応したストリーム ( `IntStream <http://lambdadoc.net/api/java/util/stream/IntStream.html>`_
`LongStream <http://lambdadoc.net/api/java/util/stream/LongStream.html>`_ `DoubleStream <http://lambdadoc.net/api/java/util/stream/DoubleStream.html>`_ )が定義されています。
Streamインタフェースはイテレータのようなもので、含まれる各要素に対して処理する必要があるときに用いられますが、
特定のおかしなメカニズムに括り付けられているわけではありません。
ストリームは、まだストリームに含まれる要素が生成され続けている、または含まれる要素を全て処理しきって
なくなってしまうような場合に特に効果を発揮します。
ストリームは、配列やコレクション、ストリームを生成する関数、またはIOチャネルから取得することができます。
また、ある処理の結果として別のストリームを取得することもあります(詳細は以下を参照してください)。
ストリームが特に役立つ場面は、多くの要素が無限に生成され続ける、例えばジェネレータ機能のようなものです。


..  
  Stream types define intermediate operations (resulting in new streams), e.g. map,
  and terminal operations (resulting in non-stream values), e.g. forEach.
  Calls on intermediate operations are often chained together in the style of a fluent API,
  forming a pipeline (as previously described).
  Terminal operations, as the name implies, terminate a method chain.
  Terminal operations are also called eager because invoking them causes them to consume values from the pipeline immediately,
  whereas intermediate operations, also called lazy, only produce values on demand.
  For example, assuming that strings has been declared as a List<String>, this code:

Stream型にはmapのような(処理結果として新たなストリームを生成する)中間オペレーションと、
forEachのような(ストリームではない値を結果を生成する)終了オペレーションが定義されています。
中間オペレーションは、(前述した)パイプライン方式の流れるような連続したAPI操作によって呼び出されます。
終了オペレーションは、その名前のとおりメソッドチェーンの終端として呼び出されます。
また、終了オペレーションはパイプライン処理の結果を処理の対象とする性質上、
プログラムが終了オペレーションに到達した時点ですぐに呼び出されます。
それに対して中間オペレーションは遅延実行され、値が必要になった時点ではじめて実行されます。
例えば、以下のコードではList<String>と見なされた文字列集合を処理しています。

.. code-block:: java
  
  IntStream ints = strings.stream().map(s -> s.length()).filter(i -> i%2 != 0);


..  
  sets up a pipeline which will first produce a stream of int values corresponding to the lengths of the elements of strings,
  then pass on the odd ones only. But none of this happens as a result of the declaration of ints.
  Processing only takes place when a statement like

まず、各文字列要素の長さを示したint値のストリームを生成し、そのうち奇数の長さだけを抽出する処理にパイプラインを用いています。
しかし、このままでは何も起こらず、処理結果でintsを取得することはありません。
intsに対して以下のような文を書いたときだけ、intsを取得するパイプライン処理が実行されます。


.. code-block:: java
  
  ints.forEach(System.out::println);


..  
  uses an eager terminal operation to pull values down the pipeline.

すぐに評価される終了オペレーションを実行することで、初めてパイプラインから値を取得しようと処理が開始されます。

..
  The following table shows a small sample of operations on Stream.
  These have been chosen for simplicity;
  also, in the same cause, bounded generic types in their signatures have been replaced by their bounds. 
  (Intermediate and terminal stream operations are listed in greater detail here (tbs) and here (tbs).)

以下の表はStreamにおける処理のサンプルの代表的な例を示しています。
なるべくシンプルなものを選んで紹介しています。
表記をシンプルするために、各シグネチャのジェネリック型の範囲からStream<T>のジェネリック型を省略し、
例えばPredicate<? extends T>をPredicate<T>と記載しています。
(中間オペレーションと終了オペレーションの詳細については `javadoc <http://lambdadoc.net/api/java/util/stream/Stream.html>`_  に記載されています。)

+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
| 操作     |引数となるインタフェース  | λ シグネチャ | 返り値型    | 返り値                                                          |
+==========+==========================+==============+=============+=================================================================+
|遅延(lazy)/仲介(intermediate) 操作例                                                                                                |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
|filter    |Predicate<T>              | T ➞  boolean | Stream<T>   | Predicateを満たす入力要素を含むストリーム                       |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
|map       |Function<T,R>             | T ➞  R       | Stream<R>   | 入力要素それぞれにFunctionを適用した結果の値のストリーム        |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
|sorted,   |Comparator<T>             | (T, T) ➞  int| Stream<T>   | Comparatorによってソートされた入力要素を含むストリーム          |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
|limit skip|                          |              | Stream<T>   | 最初のn個の入力要素のみ(またはスキップされた)を含むストリーム   |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
|即時(eager)/終端(terminal) 操作例                                                                                                   |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
|reduce    |BinaryOperator<T>         | (T, T) ➞  T  | Optional<T> | 入力要素にBinaryOperatorが適用された集約結果                    |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
|findFirst |Predicate<T>              | T ➞  boolean | Optional<T> | Predicateを満たす最初(複数)の要素                               |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+
|forEach   |Consumer<T>               | T ➞  void    | void        | voidだが、全ての入力要素にConsumerが適用される                  |
+----------+--------------------------+--------------+-------------+-----------------------------------------------------------------+


..  
  Streams may be ordered or unordered. A stream whose source is an array, a List, or a generator function, is ordered;
  one whose source is a Set is unordered. Order is preserved by most intermediate operations; 
  exceptions are sorted, which imposes an ordering whether one was previously present or not, and unordered, 
  which removes any ordering that was present on the receiver.
  (This operation is provided for situations where ordering is not significant for the terminal operation,
  but the developer wants to take advantage of the greater efficiency of some operations
  when executed in parallel on unordered stream than on ordered ones.)
  Most terminal operations respect ordering; for example toArray, called on an ordered stream,
  creates an array with element ordering corresponding to that of the stream. An exception is forEach;
  the order in which stream elements are processed by this operation is undefined.


ストリームは順序付けされる場合も、また順序付けされない場合もあります。
ストリームのソースが配列、List、ジェネレータ関数であった場合は順序付けされます。
Setがソースとなった場合は順序付けされません。

ほとんどの中間オペレーションでは順序付けが維持されますが、ソート処理は例外で、その処理自体が順序付けを目的としているため、
例えソート前に順序付けがされてあったとしても、処理の結果からは処理前の順序付け情報は失われます。
(この挙動は順序付けが終了オペレーションの結果に重要な影響を与えない場合にだけに用います。
ディベロッパがそれぞれののオペレーションをより効率的に並列処理することを望む場合は、
順序付けされていないストリームの方が順序付けストリームよりも早く処理できます。)

ほとんどの終了オペレーションは順序付けを意識します。
例えば順序付けストリームに対してtoArrayメソッドを呼び出した場合、生成される配列にはストリームの順序付けが反映されます。
例外はforEachで、ストリームに含まれる要素に対してどのような順番で処理を適用していくかは規定されていません。


