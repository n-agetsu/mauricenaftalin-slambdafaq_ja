..  Where is the Java Collections Framework going?

==========================================================
Javaコレクションフレームワークはどこに向かっていますか
==========================================================

..  
  The Java Collections Framework will be more than 15 years old by the time Java 8 ships,
  and although it has worn well, some of its underlying assumptions are increasingly called into question
  by changes in the environment in which Java programs are executed. 
  The biggest change is in the hardware environment, 
  where designers have turned their attention away from increasing the clock speed of individual cores
  and towards the goal of placing increasingly large numbers of cores on to the same chip.
  To gain the performance improvement potentially available from massively multicore processors,
  applications have to be able to divide their workload among many different threads, 
  with each thread being capable of running independently on its “ own” core.
  Library designers need to turn their attention to enabling this change,
  and since in many Java programs the most intensive activity is bulk processing of collection elements,
  the Java Collections Framework is at the center of this change.

Java 8 がリリースされる頃には、Javaコレクションフレームワークは生まれてから15年以上経過していますが、
まだまだ使われており、Javaプログラムが実行される環境の変化に対応しきれるのか疑問が生じてきています。
最も大きな変化はハードウェア環境であり、各コアのクロック数の向上や、同じチップで多くのコア数を持ち、
さらにその数を増やす傾向に対して、設計者はハードウェアをどう生かすかについて考えなければいけません。
マルチコアプロセッサの力を生かして性能向上させるためには、アプリケーションは負荷を多くのスレッドに分散させる必要があり、
また各スレッドの処理は"コア"ごとに独立して実行することで効果が得られます。
ライブラリ設計者はこれらの環境の変化に適応する必要があり、かつ多くのJavaプログラムにおける最も重い処理は
コレクション要素に対するバルク処理であることから、まずJavaのコレクションフレームワークが改修されることになりました。


..  
  Analysis of the usage of the Java collections shows one pattern to be very common,
  in which bulk operations draw from a data source (array or collection), 
  then repeatedly apply transformation operations like filtering and mapping to the data, 
  often finally summarising it in a single operation such as summing numeric elements.
  Current use of this pattern requires the creation of temporary collections to hold the intermediate results of these transformations.
  However, this style of processing can be recast to a pipeline 
  using the well-known “ Pipes and Filters” pattern, with significant resulting advantages:
  elimination of intermediate variables, reduction of intermediate storage, lazy evaluation,
  and more flexible and composable operation definitions.
  Moreover, if each operation in the pipeline is defined appropriately,
  the pipeline as a whole can often be automatically parallelised (split up for parallel execution on multicore processors). 
  The role of pipes, the connectors between pipeline operations, 
  is taken in the Java 8 libraries by implementations of the Stream interface;
  examining this interface will clarify how pipelines work.

Javaコレクションフレームワークの使い道を分析したところ、ある１つのパターンがとても多いことに気が付きました。
バルクオペレーションと呼ばれる処理です。データソース(配列やコレクション)から処理対象データを取得して、
各データに対してフィルタリングやマッピングのような変換処理を繰り返し適用し、
最後に数値要素を足し合わせるような集約処理を1度だけ行います。
バルクオペレーション処理を現在のAPIで実装するためには、各変換処理途中の値を保存するために一時コレクションを生成する必要があります。
しかしながら、このスタイルの処理は "パイプ & フィルター" パターンとして知られているパイプライン処理に置き換えることで、
多くのメリットを得ることができます。
中間生成値を不要とする、中間生成値を保存するデータ領域を削減する、遅延評価ができる、など
よりフレキシブルでコンポーネント化しやすい処理が定義できます。
さらに、パイプライン中の処理が適切に定義されていると、
パイプライン処理全体を容易に並行処理することが可能となります(マルチコア上で並行実行数を向上させることが可能)。
各パイプの役割はパイプライン処理間のコネクタであり、Java 8 ライブラリでは `Stream <http://lambdadoc.net/api/java/util/stream/Stream.html>`_ インタフェースの実装によって
このコネクタを実現し、Streamインタフェースによりパイプライン処理がどのように行われるか明瞭になります。


..  
  This is the emphasis for evolution of the Java Collections Framework in Java 8.
  It will result in a more functional style of programming collections,  
  replacing in-place mutation of values with the idioms defined by the methods of the Stream interface.

上記こそが Java 8 のJavaコレクションフレームワークの大きな進化です。
コレクションのプログラミングにより関数型のスタイルを導入し、Streamインタフェースに定義されたメソッドによって、
値の編集処理を新しいイディオムの置き換えます。

