.. What are method references?

=================================
メソッド参照とは何ですか?
=================================

..
  Any lambda expression may be thought of as an anonymous representation of a function descriptor of a functional interface.
  An alternative way representing a function descriptor is with a concrete method of an existing class.
  Method references are handles to such existing methods. For example,


ラムダ式は関数インタフェースが持つ関数ディスクリプタの匿名表現の手段として使われます。
関数ディスクリプタを表現する代わりの方法として、既存の具象メソッドを使う方法があります。
メソッド参照はこれら既存のメソッドを用いるためにあります。例えば、


.. code-block:: java
  
  String::valueOf
  Integer::compare


..
  are references to static methods, analogous to lambda expressions that do not capture any instance or local variables.
  (Instance method references are treated next.)
  For a trivial example, the method in the class java.util.Arrays


これらはstaticメソッドに対する参照を示しており、ラムダ式と同様にインスタンスやローカル変数を参照することはできません。
(インスタンスメソッド参照は次の項目で扱います。)
次のあまり面白くない例は、java.util.Arraysクラスのメソッドです。


.. code-block:: java
  
  public static <T> void sort(T[] a, Comparator c);


..  
  expects a Comparator for its second argument.
  he method Integer.compare has a signature that is type-compatible with Comparator’s function descriptor—that is,
  its compare method—so it would be legal to call Arrays.sort like this:

第2引数ではComparatorインタフェースの実装が期待されています。
この場合のInteger.compareが持つシグネチャは、Comparatorインタフェースの関数ディスクリプタと型互換性があります。
そのため、以下のようにcompareメソッドを書いてArray.sortを呼び出すことができます。


.. code-block:: java
  
  Arrays.sort(myIntegerArray, Integer::compare)


..
  In this simple example, the signature of the referenced method, Integer::compare,
  happens to match the (erased) function descriptor of Comparator.
  In general, an exact match isn’t necessary: in such a call,
  the method reference can be seen as shorthand for a lambda expression made up from a formal parameter list copied from the function descriptor
  and a body that calls the referenced method.

このシンプルな例では、メソッド参照のシグネチャは Integer::compare であり、
Comparatorの(型情報が消された)関数ディスクリプタにマッチします。
一般的に、正確に一致する必要はなく: このような呼び出しの場合においては、
メソッド参照によって、ラムダ式に関数ディスクリプタで定義された引数リストをコピーして、
参照したいメソッドを呼び出すボディ部を書くよりも短くプログラムを書くことができます。


.. (erased)とあるが、ここでの意味が不明。直訳すると消去の意味。


..
  Notice that the syntax ReferenceType::Identifier used to reference static methods as in the examples above
  can be used to reference instance methods also.


ここで確認しておきたいことには、ReferenceType::Identifier 形式の文法は上記のようなstaticメソッドの参照だけでなく、
インスタンスメソッドの参照にも使えることです。

