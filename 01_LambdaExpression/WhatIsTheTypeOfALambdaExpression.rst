.. What is the type of a lambda expression?

==============================
ラムダ式の型は何ですか
==============================

..  
  A lambda expression is an instance of a functional interface. 
  But a lambda expression itself does not contain the information about which functional interface it is implementing;
  that information is deduced from the context in which it is used. For example, the expression

ラムダ式は関数インタフェースのインスタンスです。
しかし、ラムダ式自体には関数インタフェースが実装されていることを示す情報は含まれていません。
どの関数インタフェースを実装しているかの情報は、ラムダ式が使われているコンテキストから推測します。
例えば以下のような式があるとします。


.. code-block::  java
    
     x -> 2 * x 


..  
  can be an instance of the functional interface

以下のような関数インタフェースの実装となるでしょう。


.. code-block:: java
    
    interface IntOperation { int operate(int i); }


..  
  so it is legal to write

先ほどのラムダ式を正確に書くと以下の通りです。


.. code-block:: java
    
    IntOperation iop = x -> x * 2; 


..  
  The type expected for the expression on the right-hand side of the assignment here is IntOperation.
  This is called the target type for the lambda expression. 
  Clearly a lambda expression can be type-compatible with different functional interfaces,
  so it follows that the same lambda expression can have different target types in different contexts. 
  For example, given an interface

右辺に割り当てられた式の値は、IntOperation型であることが期待されます。
これをラムダ式のターゲット型と呼びます。
ラムダ式は異なる関数インタフェースと型互換が可能であるため、同じラムダ式でも異なるコンテキストの場合は、
異なるターゲット型を持つことができます。
例えば、以下のようなインタフェースがあります。


.. code-block:: java
    
    interface DoubleOperation { double operate(double i); } 



..  
  it would also be legal to write

これもまた、正確に書くと以下のようになります。


.. code-block:: java
    
   DoubleOperation dop = x -> x * 2; 


..  
  The target type for a lambda expression must be a functional interface and,
  to be compatible with the target type, the lambda expression must have the same parameter types as the interface’s function descriptor,
  its return type must be compatible with the function descriptor,
  and it can throw only exceptions allowed by the function descriptor.

ラムダ式のターゲット型は必ず関数インタフェースであり、かつ各関数インタフェースの間にはターゲット型に対する型互換があります。
この型互換を扱う場合には、ラムダ式はインタフェースの関数ディスクリプタであるパラメータ型と同じにする必要があり、
さらに関数ディスクリプタて定義された例外のみを投げることが許されています。
