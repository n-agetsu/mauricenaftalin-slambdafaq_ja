.. What is a functional interface?

======================================
functional interface とは何ですか
======================================

..  
  Informally, a functional interface is one whose type can be used for a method parameter when a lambda is to be supplied as the actual argument. 
  For example, the forEach method on collections could have the following signature:

形式的にいうと、関数インタフェースとは1つのメソッド引数を持つ型であり、そのメソッド引数にラムダを実引数として適用する型のことを示します。
例えば、コレクションのforEachメソッドは以下のようなシグネチャを持ちます。

.. code-block:: java

  public void forEach(Consumer<? super T> consumer); 


..  
  The implementation of forEach must apply a single method of the Consumer instance that has been supplied. 
  This instance may be a lambda expression (see What is the type of a lambda expression?);
  if so, it will be applied in the place of that method. 
  A lambda expression supplied in this way can take the place of only one interface method, 
  so an interface can be used like this without ambiguity only if it has a single method.

forEachの実装においては、単一メソッドを持つConsumerインスタンスを作成し、適用することが必要です。
このメソッドに適用するインスタンスは、ラムダ式とすることも可能です。 (参照 ラムダ式の型は何ですか?);
ラムダ式は1つしかメソッドを持たないインタフェースの実装として用いることができるため、
インタフェースが単一メソッドであるかどうかの曖昧さをコードから排除できます。


..  
  More precisely, a functional interface is defined as any interface that has exactly one explicitly declared abstract method. 
  (The qualification is necessary because an interface may have non-abstract default methods.)
  This is why functional interfaces used to be called Single Abstract Method (SAM) interfaces, 
   a term that is still sometimes seen.

より正確にいうと、関数インタフェースは単一の明示的な抽象メソッド定義を持つインタフェースです。
(この条件は必要条件であり、ゆえに関数インタフェースは抽象メソッドでないデフォルトメソッドを持つことがあります。)
これが単一抽象メソッドインタフェース(Single Abstract Method : SAM)を関数インタフェースと呼ぶのかの理由であり、
この考え方は今後もたびたび出てきます。


Examples
----------

..  
  The following interfaces in the platform libraries (chosen from many) are functional interfaces according to the definition above:

以下の(多くの中から抽出した)プラットフォームライブラリに含まれるインタフェースは、関数インタフェースとして定義されます。

.. code-block:: java

  public interface Runnable { void run(); } 
  public interface Callable<V> { V call() throws Exception; } 
  public interface ActionListener { void actionPerformed(ActionEvent e); } 
  public interface Comparator<T> { int compare(T o1, T o2); boolean equals(Object obj); } 



Syntax notes
--------------

..  
  * The interface Comparator is functional because although it declares two abstract methods,
    one of these—equals— has a signature corresponding to a public method in Object.
    Interfaces always declare abstract methods corresponding to the public methods of Object,
    but they usually do so implicitly.
    Whether implicitly or explicitly declared, such methods are excluded from the count.
  
  * [Skip this note on a first reading.] The situation is complicated by the possibility that
    two interfaces might have methods that are not identical but are related by erasure.
    For example, the methods of the two interfaces 


* | Comparatorインタフェースは関数インタフェースです。しかしながら、2つの抽象メソッドを持っています。
  | その一つが -equals- のようなObjectクラスが持つシグネチャですが、通常暗黙的なものと見なされます。
  | 明示的、または暗黙的な定義だとしても、これらのメソッドは関数インタフェースとなり得る条件のカウントから除かれます。 

* | [この項目は最初は読飛ばしても良いです。] 
  | 2つのインタフェースがメソッド名で識別できずにイレイジャのみを関連とする複雑なケースもあります。
  | これら2つのインタフェースの例には以下のようなものがあります。


.. code-block:: java
  
  interface Foo1 { void bar(List<String> arg); }
  interface Foo2 { void bar(List arg); }

..  
  are said to be override-equivalent. If the (functional) superinterfaces of an interface contain override-equivalent methods,
  the function descriptor of that interface is defined as a method that can legally override all the inherited abstract methods.
  In this example, if

このようなインタフェースを等価オーバライド(override-equivalent)と言います。
(関数)スーパーインタフェースが等価オーバーライドメソッドを含む場合、インタフェースの関数ディスクリプタを、
継承した全ての抽象メソッドをオーバーライドする形式で定義することが文法上可能です。
例えば以下のようなインタフェースの場合、

.. code-block:: java
  
    interface Foo extends Foo1, Foo2 {}

..  
  then the function descriptor of Foo is

Fooの関数ディスクリプタは以下のようになります。

.. code-block:: java
    
     void bar(List arg);

..  
  In fact, every functional interface has such a function descriptor,
  though in the more common and simpler case it is just the single abstract method of that interface.

実際に、全ての関数インタフェースはこのような関数ディスクリプタを持っていますが、
一般的でシンプルなケースでは単一抽象メソッドのみを定義されていることでしょう。
