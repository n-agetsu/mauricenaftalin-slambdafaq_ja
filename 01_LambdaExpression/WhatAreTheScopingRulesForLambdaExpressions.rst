..  What are the scoping rules for lambda expressions?

=====================================================
ラムダ式のスコープのルールはどうなっていますか
=====================================================

..
  Lambda expressions do not introduce any new naming environment.
  Names in the body of a lambda are interpreted exactly as in the enclosing environment, 
  except for the addition of new names for the lambda expression’s formal parameters.
  The keywords this and super also have the same meaning as immediately outside the lambda expression
  —that is, they refer to the enclosing class.
  Formal parameters follow the same rules as method parameters for shadowing class and instance variables.
  For example, the declaration of Bar:


ラムダ式による新たな名前環境の導入はしません。
ラムダのボディ内で使われる名前は、ラムダ式の仮引数として定義された名前を除き、
エンクロージング環境 [#]_ に含まれると解釈されます。
キーワードthisやsuperはラムダ式を含む外側の環境と同じ参照、すなわちエンクロージングクラスと
同じ参照を示します。
ラムダ式の仮引数には、通常のメソッド引数に対するクラス・インスタンス変数へのシャドーイング [#]_ と
同じルールが適用されます。
例えば、Barを以下のように定義します。



.. code-block:: java
  
   class Bar { int i; Foo foo = i -> i * 2; };


..
  is legal because the lambda parameter i shadows the instance variable.
  For local variables, on the other hand, shadowing is not possible,
  so the variable is not redeclared and the usual rule of assignment before use applies,
  making the method declaration illegal.

この例はラムダ式の引数 i はインスタンス変数のシャドーであるため、正常にコンパイルされるプログラムです。
一方はローカル変数のシャドーイングはできないため、変数の再定義はできないこと、ローカル変数は値を設定する前に
使用できない2つのルールにより、以下のメソッド定義は不正であり、コンパイルは通りません。


.. code-block:: java
  
  void bar() { int i; Foo foo = i -> i * 2; };	// 不正: 変数 i は既に定義済みであるため。


.. rubric:: 訳注

.. [#] 内部クラスから見た、外部クラスのインスタンス変数等を含む環境のこと。
.. [#] クラス・インスタンス変数とローカル変数には同じ名前を付けることができ、これによりクラス・インスタンス変数が見えなくなること。
