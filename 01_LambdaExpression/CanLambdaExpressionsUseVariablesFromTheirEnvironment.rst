..  Can lambda expressions use variables from their environment?

================================================================
ラムダ式では環境に応じた変数を扱うことは可能ですか
================================================================

..
  Yes. This is called variable capture. 
  Instance and static variables may be used and changed without restriction in the body of a lambda.
  The use of local variables, however, is more restricted:
  capture of local variables is not allowed unless they are effectively final, a concept introduced in Java 8.
  Informally, a local variable is effectively final if its initial value is never changed
  (including within the body of a lambda expression)
  —in other words, declaring it final would not cause a compilation failure.
  The concept of effective finality does not introduce any new semantics to Java;
  it is simply a slightly less verbose way of defining final variables.
  The rationale for requiring captured local variables to be effectively final is explained here.

はいできます。

これを変数キャプチャを呼びます。
インスタンス変数やstatic変数をラムダのボディ部で制約なしで変更することができます。
ローカル変数を使用することもできますが、条件があります。
Java 8 から導入された概念として、実効的にfinalでないローカル変数をラムダ式から参照することはできません。
わかりやすくいうと、実効的にfinalでないローカル変数とは、初期化値が変更されないことを示します。
(ラムダ式のボディ部で変更されないことを含む)
-言い換えると、finalで定義されたローカル変数はコンパイル失敗の原因にはなりません。
実効的にfinalという概念はJavaに何ら新しい考え方を持ち込むものではなく、
finalな変数を毎回定義する煩わしさを少しだけ改善するための方法でしかありません。


