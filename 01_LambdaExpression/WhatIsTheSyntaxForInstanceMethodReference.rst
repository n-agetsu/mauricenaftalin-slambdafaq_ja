..  What is the syntax for instance method references?

=========================================================
インスタンス参照メソッドはどのような文法で表現しますか
=========================================================

..
  The syntax for referring to static methods has been described. 
  There are two ways of referring to instance methods.
  One is strictly analogous to the static case, replacing the form ReferenceType::Identifier with ObjectReference::Identifier.
  For example, the forEach method could be used to pass each element from a collection into an instance function for processing:


staticメソッドを参照する文法については既に言及しています。
ここでは、インスタンスメソッドを参照する2つの方法について解説します。
1つ目はstaticメソッドの参照と全く同じで、ReferenceType::Indentifer を ObjectReference::Identifier に置き換える方法です。
例えば、forEachメソッドにはコレクションに含まれる全ての要素に対して処理を適用するインスタンスメソッドを設定できます。


.. code-block:: java
  
   pointList.forEach(System.out::print);


..
  This is not the most useful variant of instance method references,
  however; the argument to forEach (or any other method accepting a function in this way) 
  cannot refer to the element that it is processing. 
  Rather, supposing that the elements of pointList belonged to a class TransPoint having a method

この方法は、インスタンスメソッド参照が最も効果的なやり方ではありません。
しかしながら、forEachの引数(または同じように関数を引数に取る他のメソッド)は、
処理対象の要素を参照することができません。
しかし、pointListの要素であるTransPointクラスが持つメソッドを参照したい場合、


.. code-block:: java
  
   void transpose () { int t = x; x = y; y = t; };


..
  we often want to write something of this form:

このフォームに何か書きたくなることがよくあります。


.. code-block:: java
  
   pointList.forEach(/*この要素のxとyを入れ替える*/);


..
  The second syntactic variant of instance method references supports this usage. The form

インスタンスメソッド参照の2つ目の文法は、このケースをサポートします。
フォームには

.. code-block:: java
  
   TransPoint::transpose


..
  —where a reference type rather than an object reference is used in conjunction with an instance method name
  —is translated by the compiler into a lambda expression like this:

オブジェクトの参照ではなく、参照するオブジェクトの型をインスタンス名と一緒に書きます。
この式をコンパイラがラムダ式に変換すると次のようになります。

.. code-block:: java
  
  (MyPoint pt) -> { pt.transpose(); }



..
  —that is, a lambda expression is synthesized with a single parameter that is then used as the receiver
  for the call of the instance method. So the syntax

ラムダ式は、インスタンスメソッドを呼び出すための単一の引数と組み合わされます。
よって文法は、


.. code-block:: java
  
  pointList.forEach(TransPoint::transpose);


..
  achieves the result we wanted. The same transformation can be applied to instance methods with any number of parameters;
  in each case, an extra parameter, representing the receiver, is inserted before the invocation parameters.

これで期待した結果を得ることができます。同様の変換によって、いくつかの引数を持ったインスタンスメソッドにも適用できます。
このケースでは、余分な引数は、レシーバ側のメソッド参照の起動引数の前に挿入します。

.. # 最後の1文の意味が不明瞭。



