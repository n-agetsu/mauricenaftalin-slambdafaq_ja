.. Why are lambda expressions being added to Java?

===========================================
何故Javaにラムダ式を追加するのですか？
===========================================

..
  Lambda expressions (and closures) are a popular feature of many modern programming languages.
  Amongst the different reasons for this,
  the most pressing one for the Java platform is that they make it easier to distribute processing of collections over multiple threads.
  Currently, lists and sets are typically processed by client code obtaining an iterator from the collection,
  then using that to iterate over its elements and process each in turn.
  If the processing of different elements is to proceed in parallel, it is the responsibility of the client code,
  not the collection, to organise this.

ラムダ式(およびクロージャ)は、多くのモダンなプログラミング言語において一般的な機能として取り入れられています。
ラムダ式導入の動機は言語によりそれぞれ異なり、Javaプラットフォームの場合の一番のきっかけは、
簡易なコードによってコレクションをマルチスレッド上で処理したいことでした。
現在では、ListおよびSetはコレクションからイテレータを取得するクライアントコードによって処理しており、
各要素を順番に取得し、処理していきます。
異なる要素群を並行で処理したい場合、その並行処理を構成する責務はコレクションではなく、クライアントコードにあります。


..
  In Java 8, the intention is instead to provide collections with methods that will take functions and use them,
  each in different ways, to process their elements. 
  (We will use as an example the very simple method forEach, which takes a function and just applies it to every element.)
  The advantage that this change brings is that collections can now organise their own iteration internally,
  transferring responsibility for parallelisation from client code into library code.

Java 8 では、コレクションに対して、関数を取りその関数の適用を行うメソッドにより、
各要素群ごとに異なる処理を行うことができるようになります。
(最もシンプルな例としてforEachメソッドがあり、forEachは関数を引数にとり、全ての要素に関数を適用していきます。)
この手法によるアドバンテージは、コレクション自身のイテレーション処理内部に要素に対する処理をまとめられることであり、
並行処理に関する責務をクライアントコードからライブラリコードに移せたことです。


..
  However, for client code to take advantage of this, there needs to be a simple way of providing a function to the collection methods.
  Currently the standard way of doing this is by means of an anonymous class implementation of the appropriate interface. 
  But the syntax for defining anonymous inner classes is too clumsy to be practicable for this purpose.
  
この手法にはクライアントコードにも利点があり、コレクションメソッドに対して関数をシンプルな方法で渡すことができます。
現在は、対象のインタフェースを匿名クラスによって実装することが一般的な解決策です。
しかし、コレクションを処理したいという目的に対して、匿名内部クラスを定義するやり方は文法的にとても不格好です。


..  
  For example, the forEach method on collections will take an instance of the Consumer interface and call its accept method for every element:

例えば、コレクション上のforEachメソッドは、Consumerインタフェースのインスタンスを引数に取り、コレクションの全ての要素に対してacceptメソッドを呼び出します。

.. code-block:: java
  
   interface Consumer<T> { void accept(T t); }


..  
  Suppose that we want to use forEach to transpose the x and y co-ordinates of every element in a list of java.awt.Point.
  Using an anonymous inner class implementation of Consumer we would pass in the transposition function like this:

java.awt.Pointのリストにおいて、全ての要素のxとyを入れ替えるためにforEachを使ったとしましょう。
xとyを入れ替える関数を示すConsumerインタフェースの実装を、匿名内部クラスとして以下のように実装します。

.. code-block:: java
  
   pointList.forEach(new Consumer() {
       public void accept(Point p) {
           p.move(p.y, p.x);
       }
   });


..  
  Using a lambda, however, the same effect can be achieved much more concisely:

ラムダを用いると、同じ処理をよりシンプルに表現できます。

.. code-block:: java
  
  pointList.forEach(p -> p.move(p.y, p.x)); 
