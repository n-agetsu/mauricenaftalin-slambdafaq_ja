..  What are constructor references?

=====================================
コンストラクタ参照とは何ですか
=====================================

..
  In the same way that method references are handles to existing methods,
  constructor reference are handles to existing constructors.
  Constructor references are created using syntax similar to that for method references,
  but with the method name replaced with the keyword new. For example:

既存のメソッドに対するメソッド参照を扱う場合と同様に、
既存のコンストラクタに対するコンストラクタ参照を扱うことができます。
コンストラクタ参照は、メソッド参照と同じしょうな文法で作成することができ、
メソッド名をキーワード new で置き換えた形となります。例えば:

.. code-block:: java
  
  ArrayList::new
  File::new


..  
  If the constructor is generic, type arguments can be explicitly declared before new:

コンストラクタがジェネリックであった場合、型引数はnewの前に明示的に定義することができます:


.. code-block:: java
  
  interface Factory<T> { T make(); }
  Factory<ArrayList<String>> f1 = ArrayList::<String>new;


..
  As with method references, the choice between overloaded constructors is made using the target type of the context.
  For example, the second line of the following code maps the Integer(String) constructor
  on to each element of strList (and then adds them all into a new collection using a collector):

メソッド参照と同様に、コンテキストのターゲット型によって、オーバロードされたコンストラクタを選択します。
例えば、以下のうち2行目のコードは、strListに含まれる各要素に対して Integer(String) コンストラクタをマップしています。
(そして、コレクタの使用によってコンストラクタで生成したIntegerは、新たなコレクションに全て格納されます。)


.. code-block:: java
  
  List<String> strList = Arrays.asList("1","2","3");
  List<Integer> intList = strList.stream().map(Integer::new).collect(Collectors.toList());

