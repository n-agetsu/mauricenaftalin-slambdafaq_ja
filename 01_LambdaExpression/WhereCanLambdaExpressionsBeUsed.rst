..  Where can lambda expressions be used?

=======================================
ラムダ式はどこで使うことができますか
=======================================

..  
  Lambda expressions can be written in any context that has a target type.
  The contexts that have target types are:
  
  * Variable declarations and assignments and array initializers,
    for which the target type is the type (or the array type) being assigned to;
  * Return statements, for which the target type is the return type of the method;
  * Method or constructor arguments, for which the target type is the type of the appropriate parameter.
    If the method or constructor is overloaded, 
    the usual mechanisms of overload resolution are used before the lambda expression is matched to the target type.
    (After overload resolution, there may still be more than one matching method
    or constructor signature accepting different functional interfaces with identical functional descriptors.
    In this case, the lambda expression must be cast to the type of one of these functional interfaces);
  * Lambda expression bodies, for which the target type is the type expected for the body,
    which is derived in turn from the outer target type. Consider

ラムダ式はターゲット型を持つコンテキストの中であればどこでも使うことができます。
ターゲット型を持つコンテキストには以下のようなものがあります。

* 変数の定義と代入、および配列の初期化の時。これらのターゲット型(または配列型)は代入先の型となる。

* return文の実行時。この場合のターゲット型はメソッド返り値の型となる。

* メソッドまたはコンストラクタの引数。この場合のターゲット型は引数に適用された型となる。
  メソッドやコンストラクタがオーバロードされていた場合は、ラムダ式が使われる前と同じように通常のオーバーロードの
  メカニズムによってターゲット型が決められる。
  (オーバロードの場合、関数ディスクリプタが異なる複数の関数インタフェースがラムダ式の型の候補にマッチしてしまうことがありますが、
  この場合は、ラムダ式をこれら関数インタフェースのいずれか一つに明示的にキャストする必要があります。)

* ラムダ式のボディ部は、ラムダ式の型として期待される型となり、外部ターゲット型から順番に派生した型になります。
  例えば以下の例を見てください。 

.. code-block:: java

  Callable<Runnable> c = () -> () -> { System.out.println("hi"); };


..
  The outer target type here is Callable<Runnable>, which has the function descriptor

この場合の外部ターゲット型は関数ディスクリプタを持つ Callable<Runnable> です。

.. code-block:: java
  
  Runnable call() throws Exception;


..
  so the target type of the lambda body is the function descriptor of Runnable, namely the run method. 
  This takes no arguments and returns no values, so matches the inner lambda above;

そのため、ラムダ式のボディのターゲット型はRunnableのrunメソッドの関数ディスクリプタを示します。
runメソッドは引数も返り値も持たないため、内側のラムダは上記の型にマッチします。


..
  * Ternary conditional expressions (?:), for which the target type for both arms is provided by the context.
    For example:

* 一時判定式の (?:) は、それぞれの真であった場合の式と偽であった場合の式の両方を、
  コンテキストに応じてターゲット型にするために使います。
  例えば以下の通りです。


.. code-block:: java
  
  Callable<Integer> c = flag ? (() -> 23) : (() -> 42);


..
  Cast expressions, which provide the target type explicitly. For example:

キャスト式では、明示的にターゲット型を指定します。例えば以下の通りです。

.. code-block:: java
  
  Object o = () -> { System.out.println("hi"); };		          // 不正 : RunnableにもCallableにもなり得るため (他の型も考えられる)
  Object o = (Runnable) () -> { System.out.println("hi"); };	  // 曖昧さがなくなったため適正。


