..  Are lambda expressions objects?

=================================
ラムダ式はオブジェクトですか
=================================

..  
  Yes, with a qualification: they are instances of object subtypes, but do not necessarily possess a unique identity. 
  A lambda expression is an instance of a functional interface, which is itself a subtype of Object.
  To see this, consider the legal assignments:


はい、しかし条件があります: ラムダ式のインスタンスはオブジェクト型のサブタイプですが、ユニークな識別子を持つ必要なありません。
ラムダ式は関数インタフェースのインスタンスであり、オブジェクト型のサブタイプでもあります。
以下のコードをよく見て考えてみてください。

.. the legal assignments は債券譲渡や財産分与の意味らしいが、直訳すると意味が通らない。  


.. code-block:: java

  Runnable r = () -> {};   // ラムダ式を生成し、ラムダに対する参照を変数rに割り当てる
  Object o = r;            // 通常と同じように型拡張を行う


..
  To understand the situation, it is useful to know that there are both short-term goals
  and a longer-term perspective for the implementation in Java 8. 
  The short-term goals are to support internal iteration of collections, 
  in the interests of efficiently utilising increasingly parallel hardware.
  The longer-term perspective is to steer Java in a direction that supports a more functional style of programming. 
  Only the short-term goals are being pursued at present,
  but the designers are being careful to avoid compromising the future of functional programming in Java,
  which might in the future include fully-fledged function types such as are found in languages such as Haskell and Scala.

この場合について理解するには、Java 8 実装の短期的なゴールと長期的な展望を知ることが役立ちます。
短期的なゴールはコレクションの内部イテレータをサポートすることであり、より効率的にハードウェアの並行処理性能を有効活用することです。
長期的な展望は、Javaをもっと関数型プログラミングをサポートする方針に舵を切ることです。
短期的なゴールは現在のプログラミングの進化の流れに追いつくことですが、
しかし設計者たちはJavaの関数型プログラミングの機能に対しての間違った認識、HaskellやScalaの持つような完全なる関数型の機能が
Javaには備わっていないことについて憂慮しています。


..
  The question of whether lambdas are objects must be answered on the basis of how they fit into the Java’s type system, 
  not on how they happen to be implemented at any moment. Their status as objects, 
  which stems from the fundamental decision to make them instances of interfaces,
  has both positive and negative aspects:

ラムダはオブジェクトか の問いに対する答えは、いかなる実装が行われた場合においても、
Javaの持つ型システムの原則にフィットさせなければいけません。
ラムダをオブジェクトとした場合の状態は、他のインタフェース実装のインスタンス化の考え方と
同様であり、これには良い面も悪い面もあります。


..
  * it enables lambda expressions to fit into the existing type system with relatively little disturbance;
  * lambda expressions inherit the methods of Object.
  
  But note that because lambdas do not necessarily possess a unique identity,
  the equals method inherited from Object has no consistent semantics.

* ラムダ式は従来からある型システムにフィットさせることができるが、これは少々混乱の元となっている。
* ラムダ式はObject型のメソッドを継承する。

注意しておきたいのは、ラムダはユニークな識別子を持つ必要がないため、
Object型の持つequalsメソッドの継承には意味を持たないことです。

..  consistent semantics
    一貫した考え方。has no consistent semanticsで意味を持たないと訳すのは表現が強すぎるか。
