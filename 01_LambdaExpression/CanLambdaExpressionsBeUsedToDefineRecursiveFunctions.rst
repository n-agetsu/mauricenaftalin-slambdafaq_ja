.. Can lambda expressions be used to define recursive functions?

===========================================================
ラムダ式で再帰的な関数を定義することはできますか?
===========================================================

..  
  Yes, provided that the recursive call uses a name defined in the enclosing environment of the lambda.
  This means that recursive definitions can only be made in the context of variable assignment and,
  in fact—given the assignment-before-use rule for local variables — only of instance or static variable assignment. 
  So, in the following example, factorial must be declared as an instance or static variable.

はい、できます。|
ラムダの中の閉じられた環境で定義された関数であれば再帰的に呼び出すことができます。|
これは再帰の定義が変数代入のコンテキストの中でのみ可能であることと、
ローカル変数アクセスに対するルール『ラムダ式で使用する前に値を確定させる』があることにより、
再帰関数ではインスタンス変数またはstatic変数のみ使用することが可能です。
そのため以下の例では、factorialはインスタンス変数またはstatic変数として定義される必要があります。

..
  Example

例
------

.. code-block:: java
  
  UnaryOperator<Integer> factorial = i -> { return i == 0 ? 1 : i * factorial.apply( i - 1 ); };


