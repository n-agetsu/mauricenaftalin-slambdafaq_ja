.. What is a lambda expression?

=============================
ラムダ式とは何ですか?
=============================

.. In mathematics and computing generally, a lambda expression is a function:
   for some or all combinations of input values it specifies an output value.
   Lambda expressions in Java introduce the idea of functions into the language.
   In conventional Java terms lambdas can be understood as a kind of anonymous method with
   a more compact syntax that also allows the omission of modifiers,
   return type, and in some cases parameter types as well.


数学および一般的な計算では、ラムダ式は一定のまたは全ての入力値の組み合わせに対して、
出力値を定義する関数の一種と見なされています。

Javaのラムダ式では、この関数の考え方を言語に導入しています。
Javaとしてのラムダは、匿名メソッドの定義から戻り値の型、パラメータ型を省略することによって、
もっとコンパクトな記法を実現するために使われています。


.. Syntax
   
   The basic syntax of a lambda is either


文法
-----------

一般的なラムダ式は以下の2種類があります。

.. code-block:: java

  (parameters) -> expression
  or
  (parameters) -> { statements; }


.. Examples

記述例
-----------

..  
  1. (int x, int y) -> x + y                          // takes two integers and returns their sum
  2. (x, y) -> x - y                                  // takes two numbers and returns their difference
  3. () -> 42                                         // takes no values and returns 42 
  4. (String s) -> System.out.println(s)              // takes a string, prints its value to the console, and returns nothing 
  5. x -> 2 * x                                       // takes a number and returns the result of doubling it
  6. c -> { int s = c.size(); c.clear(); return s; }  // takes a collection, clears it, and returns its previous size

.. code-block:: java

  1. (int x, int y) -> x + y                          // 2つの整数を引数に取り、合計値を返す
  2. (x, y) -> x - y                                  // 2つの数値を引数に取り、差を返す
  3. () -> 42                                         // 引数なしで、42を返す
  4. (String s) -> System.out.println(s)              // 文字列を引数に取り、コンソールに値を出力し、戻り値はなし
  5. x -> 2 * x                                       // 数値を引数に取り、2倍にした値を返す
  6. c -> { int s = c.size(); c.clear(); return s; }  // コレクションを引数に取り、クリアし、クリア前のサイズを返す
  

.. Syntax notes

文法上の注意点
----------------

..  
  ・Parameter types may be explicitly declared (ex. 1,4) or implicitly inferred (ex. 2,5,6).
    Declared- and inferred-type parameters may not be mixed in a single lambda expression.
  
  ・The body may be a block (surrounded by braces, ex. 6) or an expression (ex. 1-5).
    A block body can return a value (value-compatible, ex. 6) or nothing (void-compatible).
    The rules for using or omitting the return keyword in a block body are the same as those for an ordinary method body.
    If the body is an expression, it may return a value (ex. 1,2,3,5) or nothing (ex. 4).
    
  ・Parentheses may be omitted for a single inferred-type parameter (ex. 5,6)
   
  ・The comment to example 6 should be taken to mean that the lambda could act on a collection.
    Equally, depending on the context in which it appears,
    it could be intended to act on an object of some other type having methods size and clear,
    with appropriate parameters and return types.

* | 引数型は明示的に定義する(ex. 1,4)ことも、型推論により暗黙的とし省略することもできる。(ex 2, 5, 6)
  | 1つのラムダ式のなかで、明示的に型を定義する引数と、型を省略した引数を混ぜて使用することはできない。

* | ボディ部はブロック(波括弧{}で囲う, ex. 6)、または式(ex. 5,6)で定義する。
  | ブロックからは値を返すことができる (value-compatible, ex. 6)、または値を返さないこともできる。(void-compatible)
  | return文をブロックから省略できるかどうかのルールについては、通常のメソッドと同じである。
  | ボディ部を式とした場合、値を返すことも(ex. 1,2,3,5)、返さないこともできる(ex. 4)。

* 括弧に含まれる単一引数の型は省略することができる。(ex. 5, 6)

* | example6については特に、ラムダがコレクションに対して振る舞うことについて言及している。
  | これは、ラムダ式のコンテキストは記載された場所に依存し、適切なパラメータや返り値型を持つsizeやclearなど、
  | 通常のオブジェクトが持つメソッドと同様に振る舞うことを示している。


