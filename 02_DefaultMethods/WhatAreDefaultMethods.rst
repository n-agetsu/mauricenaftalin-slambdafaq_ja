.. What are default methods?

=======================================
デフォルトメソッドとは何ですか
=======================================

..
  The page Why are lambda expressions being added to Java? uses as an example the simple collections method forEach, 
  which takes a function and applies it to every element, for example:

:doc:`../01_LambdaExpression/WhyAreLambdaExpressionBeingAddedToJava` において、関数を引数にとって全ての要素に関数を
適用していくforEachメソッドの例を紹介しました。


.. code-block:: java
  
  pointList.forEach(p -> p.move(p.y, p.x));


..
  But the classes of the Java Collections Framework, having been designed fifteen years ago 
  for a language without a functional orientation, 
  do not have a method forEach, or any of the other methods that this strategy will require. 
  Adding these methods to the Collections Framework classes individually would destroy the coherence of the highly interface-based Framework
  —for collections classes, it is the interface that defines the contract of that client code can depend on
  (for the same reason, adding static methods to the utility class Collections is not a solution either)
  So the interfaces of the Framework need to reflect the new functionality that is being added to its collections. 
  But until now adding new methods to an interface has been impossible without forcing modification to existing classes,
  since the implements clause represents a commitment by the implementing class to override all abstract methods in the interface.
  If the interface gains a new abstract method, the implementing class must be changed to override that too.

Javaのコレクションフレームワークに含まれるクラス群は設計されてから15年以上経過しており、
当時のJava言語には関数型の考え方もなく、forEachをはじめ、関数型プログラミングで求められるメソッドがありません。
これらのメソッドをコレクションフレームワークのクラスとして個々に追加していくと、高度なインタフェースベースの
フレームワークにおける過去からの一貫性が壊れてしまいます。
コレクションクラスには、クライアントコードが依存する規約を示すインタフェースが定義されているため、
(同様の理由により、staticメソッドがコレクションのユーティティクラスに追加されてきましたが、今回の場合は解決策になりません)
フレームワークのインタフェースには、コレクションに対する機能追加により新しい関数型の考え方を反映する必要があります。
しかし、あるインタフェースに対しメソッド追加する場合、既存クラスに対して変更を加えずに済ませることはできません。
インタフェースに含まれるすべての抽象メソッドは、実装クラスにおいて全てオーバライドする約束があるためです。
インタフェースに新たな抽象メソッドが追加された場合、実装クラスは追加された抽象メソッドをオーバライドするために、
必ず変更が必要になります。


..
  This is the rationale for the introduction of default methods (also called virtual extension methods or defender methods). 
  Their purpose is to enable interfaces to evolve without introducing incompatibility with existing implementations.
  The need for them has driven a significant change in the purpose of interfaces, 
  which previously could declare only abstract methods whose semantics were defined by the contract in the JavaDoc;
  now, by contrast, they can also declare concrete methods in the form of default implementations.
  For example, Iterator could hypothetically be extended by a method that skips a single element:


この原理がデフォルトメソッド導入への動機となりました(仮想拡張メソッドやディフェンダーメソッドとも呼びます)。
デフォルトメソッド導入の目的は、既存実装クラスの互換性を維持した状態で、インタフェースの進化を可能にすることです。
この為には、インターフェースの目的である、JavaDocにも含まれる規約として抽象メソッドを定義する考え方を大きく変える必要があります。
これからは規約により、インタフェースにデフォルト実装としての具象メソッドを定義することができます。
例えば、Iteratorにおいて単一の要素をスキップするメソッドを継承する場合:


.. code-block:: java
  
  interface Iterator {
      // existing method declarations
      default void skip() {
          if (hasNext()) next();
      }
  }


..
  Suppose Iterator were extended in this way: all implementing classes now automatically expose a skip method,
  which client code can call exactly as with abstract interface methods.
  If skip is called on an instance of a class that implements Iterator without providing a body for it,
  then the default implementation in the interface declaration is invoked. Alternatively, since default methods are virtual,
  a class implementing Iterator can override the default method with a better or more implementation-specific one.

この方法においてIteratorを継承することを考えてみてください。全ての実装クラスには自動的にskipメソッドが付与され、
クライアントコードはインタフェースの持つ抽象メソッドとして呼び出すことができます。
もしskipメソッドが実装されていないIterator実装クラスのインスタンスに対してskipメソッドが呼び出された場合、
インタフェースに定義されたデフォルトメソッドが実行されます。一方でデフォルトメソッドを仮想的(virtural) [#]_ とし、
Iterator実装クラスがデフォルトメソッドをより実装クラスに沿った適切な処理にオーバライドすることもできます。

..
  The page Why do default methods need a keyword? answers a common question about the syntax of default methods.

デフォルトメソッドの文法に関するよくある質問については、 :doc:`../DesignRationale/WhyDoDefaultMethodNeedAKeyword` において解説しています。


.. rubric:: 訳注

.. [#] より下位のクラスに具象メソッドがある場合、呼び出したインスタンスの階層に最も近い実装を呼び出すこと。C++の仮想関数の考え方と同じ。
