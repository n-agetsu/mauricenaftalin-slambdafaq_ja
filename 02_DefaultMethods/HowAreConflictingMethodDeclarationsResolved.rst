.. How are conflicting method declarations resolved?

==============================================
どのように競合するメソッドを解決しますか
==============================================

..  
  Because inheritance is possible from multiple interfaces,
  the same default method can be inherited from different paths.
  Since each inherited default method provides a different implementation,
  the compiler needs a way of selecting the declaration to use.
  This is its method:

複数インタフェースを継承することが可能なことにより、異なるパスから同じデフォルトメソッドを継承することができます。
継承元のデフォルトメソッドが異なる実装を持っていた場合、コンパイラはどの宣言を使用するか選択する必要があります。
以下がその考え方です。

..  
  * Classes always win. 
    A declaration in the class or a superclass takes priority over any default method declaration.
  * Otherwise, the method with the same signature in the most specific default-providing interface is selected.
    For example, based on the rules above the following code will print Hello World from B: 


* | クラスが常に勝つ。
  | クラスおよびスーパークラスによる宣言は、他のデフォルトメソッド宣言よりも優先される。

* | それでも解決しない場合、同じメソッドシグネチャを持つ最も下位のインタフェースが提供するデフォルト実装が選択される。
  | 例えば、このルールにより以下のコードでは Hello World from B が表示される。


.. code-block:: java
  
  public interface A {
      default void hello() { System.out.println("Hello World from A"); }
  }
  public interface B extends A {
      default void hello() { System.out.println("Hello World from B"); }
  }
  public class C implements B, A {
      public static void main(String... args) {
          new C().hello();
      }
  }


..  
  Conflicts are not always avoidable. If, in the example above, interface B was declared without extending A,
  then C would inherit default methods with matching signatures from unrelated interfaces.
  Compiling that code would produce the error messages:


競合はいつも回避できるとは限りません。もし上記の例において、インタフェースBがインタフェースAを継承していない場合、
クラスCはシグネチャの合う他のインタフェースからデフォルトメソッドを継承しようと試みます。
この場合のコードをコンパイルすると、エラメッセージが表示されます。


..  
  class C inherits unrelated defaults for hello() from types A and B
  reference to hello is ambiguous, both method hello() in A and method hello() in B match.


.. code-block:: none
  
  エラー: クラス Cは型BとAからhello()の関連しないデフォルトを継承します


..
  Note that the first error would be reported whether or not there was a call to the inherited default method.
  The conflict must be resolved by overriding. 
  If desired, one of the inherited methods can be selected sing the new syntax X.super.m(...) 
  where X is the superinterface and m the method to select:

注目すべき点として、この最初のエラーは継承したデフォルトメソッドがあってもなくても出力されるものです。
競合はオーバライドによって解決する必要があります。
継承したメソッドのうち、どちらか１つを選択して呼び出す場合には新しい文法 X.super.m(...) を使用します。
X はスーパーインタフェースを示し、m は選択したメソッドを示します。　


.. code-block:: java
  
  public class C implements B, A {
      public void hello() {
          A.super.hello();
      }
      ...
  }

..  
  This now prints  Hello World from A. Note that this syntax can only be used to resolve a conflict,
  not to override either of the two principal rules listed above.


明示的な呼び出しメソッドの選択により、Hello World from A が出力されるようになりました。
この文法は競合を解決するときだけ使うことができ、上記に示したデフォルトメソッドに関するオーバライドの2つの原則を改めるものではありません。

