..  What about the diamond problem

=====================================
ダイヤモンド問題とは何ですか
=====================================

..  
  The“ diamond problem” is an ambiguity that can arise as a consequence of allowing multiple inheritance.
  It is a serious problem for languages (like C++) that allow for multiple inheritance of state. In Java, however,
  multiple inheritance is not allowed for classes, only for interfaces, and these do not contain state.


"ダイヤモンド問題"とは、多重継承を許可することによって発生する不確性を示します。
(C++のように)状態の多重継承を許可している言語では深刻な問題となります。
Javaの場合、クラスの多重継承は許可しておらず、状態を持たないインタフェースのみに多重継承を許可しているため、
それほど大きな問題ではありません。


..
  Consider the following situation:


以下のような状況を考えてみてください。


.. code-block:: java

  interface A {
      default void m() { ... }        
  }
  interface B extends A {}
  interface C extends A {}
  class D implements B, C {}


.. image:: images/Diamond.png


..  
  The rules for default method selection given on the previous page
  provide a straightforward interpretation of this scenario and its variants.


前のページ :doc:`HowAreConflictingMethodDeclarationsResolved` で解説したどのデフォルトメソッドが選択されるかのルールによって、
このシナリオをすんなりと解釈することができます。


..
  In the initial case (the code above), the implementation of m inherited by D is unambiguously
  that defined by A—there is no other possibility.
  If the situation is changed so that B now also declares a default implementation of m,
  that becomes the implementation that D inherits by the“ most specific implementation” rule. 
  But if both B and C provide default implementations, then they conflict,
  and D must provide an overriding declaration, possibly using the syntax X.super.m(...)
  in the body of m to explicitly choose one of the inherited methods.
  All three cases are clearly covered by the rules for method resolution explained in the previous answer.


(コード上の)最初のケースでは、メソッドmを継承した実装クラスDに曖昧さはなく、インタフェースaのデフォルト実装が使われます。
他に有効なメソッドがないからです。
状況を変化させ、インタフェースBにメソッドmのデフォルト実装を宣言した場合、"最も下位の実装"のルールに従ってクラスDはインタフェースBのデフォルト実装を継承します。
しかし、クラスBとCの両方がデフォルト実装を提供した場合は競合が発生し、クラスDにおいて宣言をオーバライドすることで解決する必要があります。
このクラスDにおけるオーバライドメソッドでは、文法 X.super.m(...) によって継承する上位インタフェースのメソッドを明示的に選択することも可能です。
前ページで解説したメソッド解決のルールが3つのケースすべてを明確にカバーしています。

..  
  Default methods are virtual, like all methods in Java. This can sometimes lead to surprising results. Given the declarations

デフォルトメソッドは、Javaの全てのメソッドのように仮想的なものです。
この考え方により驚くべき結果をもたらすことがあります。以下の宣言について考えてみます。


.. code-block:: java
  
    interface A {
        default void m() { System.out.println("hello from A"); }
    }
    interface B extends A {
        default void m() { System.out.println("hello from B"); }
    }
    interface C extends A {}
    class D implements B, C {}


..  
  the code

以下のコードにおいては、

.. code-block:: java
  
    C c = new D();
    c.m();

..  
  will print hello from B. The static type of c is unimportant; what counts is that it is an instance of D,
  whose most specific version of m is inherited from B.

hello from B が出力されます。静的型Cはあまり重要ではありません。
Dのインスタンスを基準に、最も下位で具象クラスに近いBクラスから継承されるmメソッドが選択されます。
