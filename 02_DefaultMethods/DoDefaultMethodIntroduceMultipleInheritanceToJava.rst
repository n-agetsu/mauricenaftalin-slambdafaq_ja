.. Do default methods introduce multiple inheritance to Java?

===========================================================
デフォルトメソッドによりJavaにも多重継承が導入されますか
===========================================================

..
  No, because multiple inheritance is already present in Java.
  Multiple inheritance of interface types has been a feature of the language since its beginning. 
  Default methods do introduce a new kind of multiple inheritance, namely multiple inheritance of behaviour. 
  Java will still not have multiple inheritance of state, as for example C++ has.

いいえ、Javaには多重継承が既にあります。
インタフェース型による多重継承は、Java言語がリリースされた当初から存在する機能です。
デフォルトメソッドにより『振る舞いの多重継承』呼ばれる、新たな多重継承の考え方が導入されます。
JavaにはC++のような状態の多重継承はありません。


..
  Here is an example of multiple inheritance of behaviour. 
  The new interface java.util.Sized declares methods size and isEmpty and provides a default implementation of the latter:


『振る舞いの多重継承』の例を以下に示します。
新しいインタフェース java.util.Sized にはsizeメソッドと、デフォルト実装が提供されるisEmplyメソッドが宣言されています。


.. code-block:: java
  
  public interface Sized { 
      public default boolean isEmpty() { 
          return size() == 0; 
      } 
      public int size(); 
  } 


..
  The new interface java.util.Traversable<T> declares the method forEach and provides a default implementation:

新しいインタフェース java.util.Traversable<T> にはforEachメソッドが宣言され、デフォルト実装が提供されています。

.. code-block:: java

  public interface Traversable<T> { 
      public default void forEach(Block<? super T> block) { 
          for (T t : this) { 
              block.apply(t); 
          } 
      } 
  }


..  
  Suppose we now declare a class SimpleCollection<T> and provide it with implementations of iterator and size:

iteratorメソッドをsizeメソッドの実装を持つクラス SimpleCollection<T> の宣言について考えてみてください。


.. image:: images/MultipleInheritanceWithMethods.png

.. code-block:: java
  
    class SimpleCollection<T> implements Sized, Traversable<T> { 
        public Iterator<T> iterator() { ... } 
        public int size() { ... } 
    }


..
  then, given a declaration

以下のように宣言します。


.. code-block:: java
  
  SimpleCollection<String> sc = new SimpleCollection<>();

..
  the following statements all compile:


以下の文はすべてコンパイル可能です。


.. code-block:: java
  
  Sized s = sc;                        // SimpleConnectionはSizedの派生クラス
  Traversable<String> t = sc;          // SimpleConnectionはTraversableの派生クラス
  System.out.println(sc.isEmpty());    // isEmplyはSizedのデフォルト実装が有効となる
  sc.forEach(System.out::println);     // forEachはTraversableのデフォルト実装が有効となる
