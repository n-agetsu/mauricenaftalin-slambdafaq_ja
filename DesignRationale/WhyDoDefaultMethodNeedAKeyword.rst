..  Why do default methods need a keyword?

=================================================
なぜデフォルトメソッドには予約語が必要ですか?
=================================================

..  
  It is often asked why the designers felt it necessary to distinguish default methods with a keyword,
  given that it can—and must—be applied only to non-abstract interface methods
  (and that the same keyword is already used, with a different meaning, as part of the switch statement).
  A minimal syntax would dispense with the keyword.
  This issue was debated extensively during the design of the language support for lambdas;
  a flavour of the discussion can be gained from many of the 76 posts in the thread “ Default Method Survey Results” 
  in the August 2012 lambda-dev archive.
  The conclusion that the Expert Group reached is summarised in a later post:
  the central reason is that a keyword immediately prompts a reader’s understanding,
  in the same way as the strictly redundant keyword abstract does for abstract methods and classes.
  Since the keyword default is also strictly redundant, its inclusion clearly depended finally on subjective judgements.

どうしてラムダの設計者たちは、デフォルトメソッドと識別するために予約が必要だったの、
非抽象インタフェースメソッドで対応できた、- そうすべきだった - とよく聞かれます。
(さらにデフォルトメソッドの予約語は既にswitch文の一部で使用されていて、別の意味も持っていたのでなおさら。)
最小の構文は、予約語を使わないことです。

この問題はラムダの言語としてのサポートを設計する間、ずっと議論になっていました。
この議論の一環として、"Default Method Survey Results"というスレッドに対して76もの投稿があり、
`ラムダ開発者メーリングリストの2012年8月分のアーカイブ <http://mail.openjdk.java.net/pipermail/lambda-dev/2012-August/005565.html>`_ として参照できます。

.. a flavor of the discussion 議論の味。議題の一環と訳してみた。

メーリングリストに投稿された内容をエキスパートグループで集約した結果、
予約語でデフォルトメソッドを表現する一番の理由は、コードを読む人が直感的に理解できること、
抽象メソッドや抽象クラスに、厳密にいうと余計な予約語abstractを付与することでわかりやすくなることと
同じであるという結論に至りました。

予約語defaultも同様に厳密には余計なものですが、明瞭性が増すかどうかは最終的に主観的な判断が下されています。

