..  Why are Stream operations not defined directly on Collection?

=============================================================
なぜStreamに関する操作はCollectionに直接定義しないのですか?
=============================================================

..  
  Early drafts of the API exposed methods like filter, map, and reduce on Collection or Iterable.
  However, user experience with this design led to a more formal separation of the “ stream” methods into their own abstraction.
  Reasons included:

アーリードラフトの段階では、filter、map、reduceといったAPIはCollectionやIterableに含まれていました。
しかしながら、実際にこのAPIを使ってみると、各APIを抽象的に示した"stream"メソッドとして分離した方がより適切だと考えました。
これは、以下のような理由があります:

..  
  * Methods on Collection such as removeAll make in-place modifications,
    in contrast to the new methods which are more functional in nature. 
    Mixing two different kinds of methods on the same abstraction forces the user to keep track of which are which.
    For example, given the declaration


* CollectionにremoveAllのようなメソッドが含まれることは自然なことですが、
  対照的に新たなメソッドはより本質的に関数的なものでした。
  2つの異なる種類のメソッドを同じ抽象化に含めてしまうと、
  ユーザがそれぞれのメソッドがどのような抽象化を示しているのか覚えておく必要があります。
  例えば、以下のような宣言があったとします。


.. code-block:: java
  
  Collection strings;


..  
  the two very similar-looking method calls

以下の2つのメソッド呼び出しは非常に良く似ています。


.. code-block:: java
  
  strings.removeAll(s -> s.length() == 0);
  strings.filter(s -> s.length() == 0);          // 現在のAPIではこのやり方はサポートされていない


..  
  would have surprisingly different results;
  the first would remove all empty String objects from the collection,
  whereas the second would return a stream containing all the non-empty Strings,
  while having no effect on the collection.

しかし結果は驚くことに全く異なります。
最初の文の結果は全ての空文字をコレクションから削除しますが、
2番目の文はコレクションに含まれる全ての空文字を抽出したストリームを返し、
コレクション自体には何ら影響を与えません。

..  
  Instead, the current design ensures that only an explicitly-obtained stream can be filtered:

引き換えにに現在の設計では、明示的にフィルタ可能なストリームが取得できることを示しています。

.. code-block:: java
  
   strings.stream().filter(s.length() == 0)...;

..  
  where the ellipsis represents further stream operations, ending with a terminating operation.
  This gives the reader a much clearer intuition about the action of filter;

この例ではfilterより後のストリームオペレーションや、終端オペレーションは省略されています。
ストリームによって、コードを読む人がフィルター処理であることを直感的に理解できます。


..  
  * With lazy methods added to Collection, users were confused by a perceived
    —but erroneous—need to reason about whether the collection was in
   “ lazy mode” or “ eager mode”. Rather than burdening Collection with new and different functionality,
    it is cleaner to provide a Stream view with the new functionality;

* Collectionに遅延メソッドを追加すると、ユーザはその処理の理解に混乱し、
  コレクションが "レイジーモード" と "イーガーモード" のどちらで処理されるかを誤って認識します。
  コレクションに新しい、かつ今までとは異なる機能性を持たせることが負担となるため、
  Streamを導入することで機能性、すなわち遅延実行であることを明らかにします。

..  
  * The more methods added to Collection, the greater the chance of name collisions with existing third-party implementations.
    By only adding a few methods (stream, parallel) the chance for conflict is greatly reduced;

* Collectionに対してさらにメソッドを追加することで、既存のサードパーティーの実装クラスと名前の衝突が起こる可能性が高まります。
  追加するメソッドを抑えることで(stream, parallel)、競合する可能性を大きく下げることができます。


..  
  * A view transformation is still needed to access a parallel view;
    the asymmetry between the sequential and the parallel stream views was unnatural. 
    Compare, for example

*  今まで並行処理を行うためには視点の切り替えが必要でした。
   ラムダが導入される以前では、シーケンシャルな処理を行うコードと、パラレルに処理を行うコードは、
   やることが同じであっても全く別のようなコードに見えました。
   ラムダ導入以前と比較して、以下の例をみてください。


.. code-block:: java

  coll.filter(...).map(...).reduce(...);
    with
  coll.parallelStream().filter(...).map(...).reduce(...);


..  
  This asymmetry would be particularly obvious in the API documentation,
  where Collection would have many new methods to produce sequential streams,
  but only one to produce parallel streams, which would then have all the same methods as Collection.
  Factoring these into a separate interface, StreamOps say, would not help;
  that would still, counterintuitively, need to be implemented by both Stream and Collection;

シリアル処理とパラレル処理の非対称性についてはAPIドキュメントに明確に記載されています。
Collectionにはシーケンシャルなストリームを処理する多くの新しいメソッドが追加されますが、
パラレルなストリーム処理を目的に追加されたメソッドは1つだけです。
CollectionとStreamインタフェースが分離された要因が、
JavaDocの `Stream Operations <http://download.java.net/jdk8/docs/api/java/util/stream/package-summary.html#StreamOps>`_  
の章で述べられていますが、この要因を理解するにはあまり役立たないと思います。
StreamとCollectionの両方を実装する必要性がまだ直感に反しているからです。

..  
  which would then have all the same methods as Collection. の意味がわからない。
  StreamがCollectionが持つメソッドを全て兼ね備えると読み取れて、DeveloperPreview時点のAPIと合わない。

..  
  * A uniform treatment of views also leaves room for other additional views in the future.

* 今後、新しい観点を思い付くためにお手洗いに席を立ったとしても、コードは毎回同じように見えるぐらい明白となるでしょう。
