.. Why are Stream operations not defined directly on Collection?

======================================================
なぜローカル変数へのアクセスには制約があるのですか?
======================================================

..
  Capture of local variables is restricted to those that are effectively final.
  Lifting this restriction would present implementation difficulties,
  but it would also be undesirable;
  its presence prevents the introduction of a new class of multithreading bugs involving local variables.
  Local variables in Java have until now been immune to race conditions and visibility problems
  because they are accessible only to the thread executing the method in which they are declared.
  But a lambda can be passed from the thread that created it to a different thread,
  and that immunity would therefore be lost if the lambda,
  evaluated by the second thread, were given the ability to mutate local variables.
  Even the ability to read the value of mutable local variables from a different thread
  would introduce the necessity for synchronization or the use of volatile in order to avoid reading stale data.

ローカル変数のキャプチャには、対象のローカル変数が実質的にfinalであることを求める制約があります。
この制約によって実装に難点を引き起こすこともありますが、必ずしも悪影響だけではありません。
ラムダ式を用いて作成した新しいクラスに、ローカル変数へのマルチスレッドアクセスによるバグが混入することを防ぐことができます。
今までのJavaのローカル変数には競合状態や変数可視性問題に対する免疫があります。
これはローカル変数に対して、変数が定義されたメソッドを実行したスレッドのみがアクセスできる制約によるものです。
しかしラムダでは、ローカル変数を生成したスレッドとは異なるスレッドからアクセスすることが可能で、
第2のスレッドによって評価されたラムダが、ミュータブルなローカル変数にアクセスすることができてしまうと、
せっかくの免疫が失われてしまいます。
また、ミュータブルなローカル変数を複数のスレッドが読み込み可能となることで、
同期化や、古いデータを読み込むことを防止するためにvolativeの使用を考慮する必要がでてきます。
 
..  
  An alternative way to view this restriction is to consider the use cases that it discourages.
  Mutating local variables in idioms like this:

別の視点でこの制約を見てみると、残念なユースケースが思い浮かびます。
ミュータブルなローカル変数を用いた以下のようなイディオムです。

 
.. code-block:: java
  
  int sum = 0;
  list.forEach(e -> { sum += e.size(); });  // 不正; ローカル変数 'sum' は、実質的にfinalではない。
 
..  
  frustrates a principal purpose of introducing lambdas.
  The major advantage of passing a function to the forEach method is that it allows strategies
  that distribute evaluation of the function for different arguments to different threads.
  The advantage of that is lost if these threads have to be synchronized to avoid reading stale values of the captured variable.

ラムダを導入することで、ユーザをイライラさせることになっています。
forEachメソッドによってリストに関数を適用する大きな利点は、各要素を引数として、
引数ごとに異なるスレッドで関数を評価する戦略が可能であることです。
これらのスレッドにおいて、キャプチャされた変数へのアクセスをコントロールするために同期化が必要になると、
ラムダの利点が失われてしまいます。

 
..  
  The restriction of capture to effectively immutable variables is intended to direct developers’
  attention to more easily parallelizable, naturally thread-safe techniques.
  For example, in contrast to the accumulation idiom above, the statement

キャプチャ対象を実質的にイミュータブル(不変)な変数に限定することで、
開発者達がもっと簡単に並列処理ができることや、本来必要なスレッドセーフテクニックに集中させることを意図しています。
例えば、対照的な集約処理のイディオムを考えてみましょう。
 
.. code-block:: java
  
  int sum = list.map(e -> e.size()).reduce(0, (a, b) -> a+b);

..  
  creates a pipeline in which the results of the evaluations of the map method can much more easily be executed in parallel,
  and subsequently gathered together by the reduce operation.

簡易に並行実行可能なmapメソッドによって評価された結果を含むパイプラインが生成され、
続けて実行されるリデュース操作によって結果が集約されます。

..  
  The restriction on local variables helps to direct developers using lambdas aways from idioms involving mutation;
  it does not prevent them. Mutable fields are always a potential source of concurrency problems if sharing is not properly managed;
  disallowing field capture by lambda expressions would reduce their usefulness without doing anything to solve this general problem.

ローカル変数に対する制約は、開発者がラムダを用いるときにミュータブルな変数を保護する必要性を意識させないことが目的です。
ミュータブルなフィールドもまた、適切な管理を行わずに値を共有することにより、並行性の問題の原因となることが多くあります。
しかし、ラムダ式からフィールドのキャプチャを制限してしまうと、どんな対処を行ってもラムダ式自体の有用性が失われてしまうため、
フィールドアクセスに対しては制約を設けていません。

..
  without doing anything to solve this general problem. の意味がよくわからない。
