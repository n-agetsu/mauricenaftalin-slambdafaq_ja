.. Why are lambda expressions so-called?

====================================
なぜラムダ式と呼ぶのですか?
====================================

..  
  The idea of lambda expressions comes from a model of computation invented
  by the American mathematician Alonzo Church, 
  in which the Greek letter lambda represents functional abstraction.
  But why that particular letter? Church seems to have liked to tease:
  asked about his choice, his usual explanation involved accidents of typesetting, 
  but in later years he had an alternative answer: “ Eeny, meeny, miny, moe.”

ラムダ式のアイディアは計算モデルの世界においてアメリカの数学者
アロンゾ・チャーチにより考案されたもので、ギリシャ文字のラムダによって関数を抽象化しています。
なぜラムダ(λ )って文字を選んだのかって? 
チャーチは人をからかうのが好きで、組み版時のアクシデントでラムダになったといつも答えてたよ。
数年後、彼から別の答えが帰ってきてね: "どちらにしようかな 天の神様の言う通り ..." で決めたってっさ。


..
  "Eeny, meeny, miney, moe. は日本でいう どちらにしようかな .. の英語版らしい"

..  
  References:


参考情報:

https://mail.mozilla.org/pipermail/es-discuss/2008-December/008520.html

http://www.lisazhang.ca/2012/07/ikea-shopping-and-calculus.html

http://www.wisdomandwonder.com/link/3022/why-church-chose-lambda
