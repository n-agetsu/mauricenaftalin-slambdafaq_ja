.. Lambdas and closures — what’s the difference?

=============================================
ラムダとクロージャ - 違いはなんですか?
=============================================

..  
  A closure is a lambda expression paired with an environment that binds each of its free variables to a value.
  In Java, lambda expressions will be implemented by means of closures,
  so the two terms have come to be used interchangeably in the community.

クロージャは、自由変数の値を環境に束縛するかしないかでラムダ式と対を成しており
Javaにおいては、ラムダ式はクロージャの実装を意味しています。
その為、コミュニティにおいてこれら2つの用語は相互に同様な意味で用いられています。

